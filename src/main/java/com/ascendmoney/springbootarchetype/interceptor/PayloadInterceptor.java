package com.ascendmoney.springbootarchetype.interceptor;
import com.ascendmoney.springbootarchetype.constant.HttpHeaderConstant;
import com.ascendmoney.springbootarchetype.constant.LoggerConstant;
import com.ascendmoney.springbootarchetype.constant.PayloadConstant;
import com.ascendmoney.springbootarchetype.domain.Payload;
import com.ascendmoney.springbootarchetype.domain.UserType;
import org.slf4j.LoggerFactory;
import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.MDC;

@Component
public class PayloadInterceptor extends HandlerInterceptorAdapter {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Payload payload = new Payload();
        Integer userId = ((request.getHeader(PayloadConstant.USER_ID_KEY) == null) ? null : Integer.parseInt(request.getHeader(PayloadConstant.USER_ID_KEY)));
        Integer userTypeId = ((request.getHeader(PayloadConstant.USER_TYPE_ID) == null) ? null : Integer.parseInt(request.getHeader(PayloadConstant.USER_TYPE_ID)));
        payload.setUserId(userId);
        UserType userType = new UserType();
        userType.setId(userTypeId);
        userType.setName(request.getHeader(PayloadConstant.USER_TYPE_NAME));
        payload.setUserType(userType);
        payload.setAccessToken(request.getHeader(PayloadConstant.ACCESS_TOKEN_KEY));
        payload.setClientId(request.getHeader(PayloadConstant.CLIENT_ID));
        payload.setDeviceId(request.getHeader(PayloadConstant.DEVICE_ID));
        payload.setDeviceDescription(request.getHeader(PayloadConstant.DEVICE_DESCRIPTION));

        request.setAttribute(PayloadConstant.PAYLOAD_KEY, payload);

        String correlationId = request.getHeader(HttpHeaderConstant.CORRELATION_ID_HEADER);
        ThreadContext.put(LoggerConstant.CORRELATION_ID_LOG_KEY_NAME, correlationId);
        ThreadContext.put(LoggerConstant.IP_ADDRESS_LOG_KEY_NAME , request.getHeader(HttpHeaderConstant.X_IP_ADDRESS));

        logger.info("payload from [{}] user id, user type is [{}], client id [{}], device is [{}] and device description is [{}]", payload.getUserId(), payload.getUserType().getName(), payload.getClientId(), payload.getDeviceId(), payload.getDeviceDescription());
        return true;
    }
}
