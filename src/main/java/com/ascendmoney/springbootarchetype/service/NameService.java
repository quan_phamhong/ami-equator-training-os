package com.ascendmoney.springbootarchetype.service;

import org.springframework.stereotype.Service;


@Service
public class NameService {
    public String getUpperCaseName(String name) {
        return name.toUpperCase();
    }
}
