package com.ascendmoney.springbootarchetype.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import org.springframework.stereotype.Service;

@Service
public class CurrencyService {
    public BigDecimal scaleAmountHalfUp(BigDecimal input, int numberOfDecimalPlace){
        if(input == null){
            return null;
        }
        return input.setScale(numberOfDecimalPlace, RoundingMode.HALF_UP);
    }
}