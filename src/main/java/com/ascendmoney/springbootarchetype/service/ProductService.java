package com.ascendmoney.springbootarchetype.service;
import org.springframework.data.repository.CrudRepository;

import com.ascendmoney.springbootarchetype.domain.Product;


public interface ProductService extends CrudRepository<Product, Integer> {
	
}
