package com.ascendmoney.springbootarchetype.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ascendmoney.springbootarchetype.domain.Product;
import com.ascendmoney.springbootarchetype.service.ProductService;

@RestController
@RequestMapping("/product")
public class TestController {
	@Autowired
	private ProductService productservice;
	
	@GetMapping(value="/getAllProduct", produces=MediaType.APPLICATION_JSON_VALUE)
	public String getAllProduct () {
		System.out.println("vao day");
		List<Product> listProducts = (List<Product>) productservice.findAll();
		return "sucess";
	}
	
}
