package com.ascendmoney.springbootarchetype.constant;

public class LoggerConstant {
    public static final String CORRELATION_ID_LOG_KEY_NAME = "CorrelationId";
    public static final String IP_ADDRESS_LOG_KEY_NAME = "IPAddress";
}