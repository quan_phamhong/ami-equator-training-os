package com.ascendmoney.springbootarchetype.constant;

public class HttpHeaderConstant {
    public static String CORRELATION_ID_HEADER = "correlation_id";
    public static String X_IP_ADDRESS = "X-IP-ADDRESS";
}
