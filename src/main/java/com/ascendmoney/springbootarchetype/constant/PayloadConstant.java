package com.ascendmoney.springbootarchetype.constant;

public class PayloadConstant {

    public static final String PAYLOAD_KEY = "payload";
    public static final String USER_ID_KEY = "user_id";
    public static final String USER_TYPE_NAME = "user_type_name";
    public static final String USER_TYPE_ID = "user_type_id";
    public static final String ACCESS_TOKEN_KEY = "access_token";
    public static final String CLIENT_ID = "client_id";
    public static final String DEVICE_ID = "device_id";
    public static final String DEVICE_DESCRIPTION = "device_description";
}
