package com.ascendmoney.springbootarchetype.domain;

public class UserType {

    private Integer id;
    private String name;

    public UserType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public UserType(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}